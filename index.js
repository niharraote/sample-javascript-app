// selecting DOM elements
const firstNameInput = document.querySelector("#first-name");
const lastNameInput = document.querySelector("#last-name");

const submitButton = document.querySelector("#add-btn");
const resetButton = document.querySelector(".reset");

const employeeList = document.querySelector("#employee-list");

// list of employees
let employees = [];

window.addEventListener('load', () => {
    // check if there are any employees in local storage
    let localContent = JSON.parse(localStorage.getItem("names"));

    if (localContent) {
        // copy the contents from local storage to the list of employees
        localContent.map(item => {
            employees.push(new Employee(item.firstName, item.lastName));
        });

        // update the list of employees in the DOM
        showEmployees();
    }
});

class Employee {
    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    get getFirstName() {
        return this.firstName;
    }

    get getLastName() {
        return this.lastName;
    }
}

function addEmployee(e) {
    // prevent default behavior of form, which is to try and submit the contents of the form inputs to a server and reload the page
    e.preventDefault();

    // validation
    if (firstNameInput.value.length < 1 || lastNameInput.value.length < 1) {
        alert ("Employee name cannot be blank!!");
    } else {
        // create a new Employee object, add it to the employees list,
        // add it to the local storage for data persistence, clear the form inputs, and update the list of employees in the DOM
        let emp = new Employee(firstNameInput.value, lastNameInput.value);

        employees.push(emp);
        
        localStorage.setItem("names", JSON.stringify(employees));
        
        firstNameInput.value = "";
        lastNameInput.value = "";

        showEmployees();
    }
}

function showEmployees() {
    // reset the employee list in the DOM every time this function executes. This prevents duplicate entries
    employeeList.innerHTML = null;

    // checks if there are any elements in the employees array. Creates a list element and appends it to the unordered list in the DOM
    if (employees) {
        employees.forEach(employee => {
            let li = document.createElement('li');
            li.innerHTML = `${employee.getFirstName} ${employee.getLastName}`;
            employeeList.appendChild(li);
        });
    }
}

function clearStorage(e) {
    // prevent default form behavior because the form, for some reason, considers every button press as a submmit action
    e.preventDefault();

    // reset everything
    localStorage.clear();
    employees = [];
    showEmployees();
}

// button click listeners
submitButton.addEventListener('click', addEmployee);
resetButton.addEventListener('click', clearStorage);
